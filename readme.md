
## Task7
We would like to asku you to write a piece of code that will flatten an array of arbitrarily nested arrays of integers into one flat array of integers. For example script of the following input

a. [[1,2[3]],4]

b. should produce the following array [1,2,3,4]


## Development 
Run gulp for a dev server. The app will automatically reload if you change any of the source files.

## Build
Run gulp build to build the project. The build artifacts will be stored in the dist/ directory.
