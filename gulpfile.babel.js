import gulp from "gulp";
import browserSync from "browser-sync";
import sass from "gulp-sass";
import sourcemaps from "gulp-sourcemaps";
import autoprefixer from "gulp-autoprefixer";
import cleanCSS from "gulp-clean-css";
import uglify from "gulp-uglify";
import concat from "gulp-concat";
import imagemin from "gulp-imagemin";
import changed from "gulp-changed";
import htmlReplace from "gulp-html-replace";
import htmlMin from "gulp-htmlmin";
import del from "del";
import sequence from "run-sequence";
import babel from "gulp-babel";
import wait from "gulp-wait";
import errorHandler from "gulp-error-handle";

const config = {
  dist: "dist/",
  src: "src/",
  cssin: "src/css/**/*.css",
  jsin: "src/js/script.js",
  imgin: "src/img/**/*.{jpg,jpeg,png,gif}",
  htmlin: "src/*.html",
  scssin: "src/scss/**/*.scss",
  cssout: "dist/css/",
  jsout: "dist/js/",
  imgout: "dist/img/",
  htmlout: "dist/",
  scssout: "src/css/",
  cssoutname: "style.css",
  jsoutname: "script.js",
  cssreplaceout: "css/style.css",
  jsreplaceout: "js/script.js",
  es2015in: "src/js/es2015/**/*.js",
  es2015out: "src/js/",
  fontsin: "src/fonts/*",
  fontsout: "dist/fonts"

};

gulp.task("reload", () =>{
  browserSync.reload();
});

gulp.task("serve", ["sass"], () =>{
  browserSync({
    server: config.src
  });

  gulp.watch([config.htmlin, config.es2015in], ["reload"]);
  gulp.watch(config.scssin, ["sass"]);
  gulp.watch(config.es2015in, ["es2015"]);
});

gulp.task("copyfonts", () =>{
  gulp.src(config.fontsin)
    .pipe(gulp.dest(config.fontsout));
});


gulp.task("sass", () =>{
  return gulp
    .src(config.scssin)
    .pipe(wait(500))
    .pipe(sass().on("error", sass.logError))
    .pipe(
      autoprefixer({
        browsers: ["last 3 versions"]
      })
    )
    .pipe(gulp.dest(config.scssout))
    .pipe(browserSync.stream());
});

gulp.task("css", () =>{
  return gulp
    .src(config.cssin)
    .pipe(concat(config.cssoutname))
    .pipe(cleanCSS())
    .pipe(gulp.dest(config.cssout));
});

gulp.task("es2015", () =>{
  return gulp
    .src(config.es2015in)
    .pipe(errorHandler())
    .pipe(babel())
    .pipe(concat(config.jsoutname))
    .pipe(uglify())
    .pipe(gulp.dest(config.es2015out));
});

gulp.task("js", () =>{
  return gulp
    .src(config.jsin)
    .pipe(gulp.dest(config.jsout));
});

gulp.task("img", () =>{
  return gulp
    .src(config.imgin)
    .pipe(changed(config.imgout))
    .pipe(imagemin())
    .pipe(gulp.dest(config.imgout));
});

gulp.task("html", () =>{
  return gulp
    .src(config.htmlin)
    .pipe(
      htmlReplace({
        css: config.cssreplaceout,
        js: config.jsreplaceout
      })
    )
    .pipe(
      htmlMin({
        sortAttributes: true,
        sortClassName: true,
        collapseWhitespace: true
      })
    )
    .pipe(gulp.dest(config.dist));
});

gulp.task("clean", () =>{
  return del([config.dist]);
});

gulp.task("build", () =>{
  sequence("clean", ["html", "js", "css"]);
});

gulp.task("default", ["serve"]);
